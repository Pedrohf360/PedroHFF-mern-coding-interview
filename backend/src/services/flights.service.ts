import { FlightsModel } from '../models/flights.model'

export class FlightsService {
    async getAll() {
        return await FlightsModel.find()
    }

    async updateStatus(code: string, value: any) {
        return await FlightsModel.updateOne(
            { code: code },
            { $set: { status: value } }
        )
    }
}
