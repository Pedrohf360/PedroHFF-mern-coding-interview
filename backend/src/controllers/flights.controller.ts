import { JsonController, Get, Put, Param, Body } from 'routing-controllers'
import { FlightsService } from '../services/flights.service'

const flightsService = new FlightsService()

@JsonController('/flights', { transformResponse: false })
export default class FlightsController {
    @Get('')
    async getAll() {
        return {
            status: 200,
            data: await flightsService.getAll(),
        }
    }

    @Put('/flight/:code')
    async updateStatus(@Param('code') code: string, @Body() value: any) {
        return {
            status: 200,
            data: await flightsService.updateStatus(code, value.status),
        }
    }
}
