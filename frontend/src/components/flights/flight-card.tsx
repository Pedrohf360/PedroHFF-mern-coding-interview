import React, { ChangeEvent } from "react";
import { Box, Card, MenuItem, Select, Typography } from "@material-ui/core";

import { FlightStatuses } from "../../models/flight.model";
import { HttpClient } from "../../clients/base-http.client";

interface FlightCardProps {
  code: string;
  origin: string;
  destination: string;
  passengers?: string[];
  status: FlightStatuses;
}

const mapFlightStatusToColor = (status: FlightStatuses) => {
  const mappings = {
    [FlightStatuses.Arrived]: "#1ac400",
    [FlightStatuses.Delayed]: "##c45800",
    [FlightStatuses["On Time"]]: "#1ac400",
    [FlightStatuses.Landing]: "#1ac400",
    [FlightStatuses.Cancelled]: "#ff2600",
  };

  return mappings[status] || "#000000";
};

export const FlightCard: React.FC<FlightCardProps> = (
  props: FlightCardProps
) => {
  const handleChange = async (
    e: ChangeEvent<{ name?: string | undefined; value: unknown }>
  ) => {
    const client = new HttpClient();
    await client.put(`http://3001/flight/${props.code}`, {
      status: e.target.value,
    });
  };

  return (
    <Card
      style={{
        backgroundColor: "#f5f5f5",
        margin: "15px",
        padding: "35px",
        justifyContent: "center",
      }}
    >
      <Box style={{ display: "flex", justifyContent: "space-between" }}>
        <Typography variant="h5">{props.code} </Typography>
        <Typography style={{ color: mapFlightStatusToColor(props.status) }}>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={props.status}
            label="Age"
            onChange={async (e) => await handleChange(e)}
          >
            <MenuItem value={"Arrived"}>Arrived</MenuItem>
            <MenuItem value={"Landing"}>Landing</MenuItem>
            <MenuItem value={"On Time"}>On Time</MenuItem>
            <MenuItem value={"Delayed"}>Delayed</MenuItem>
            <MenuItem value={"Cancelled"}>Cancelled</MenuItem>
          </Select>
        </Typography>
      </Box>

      <Box>
        <Typography>Origin: {props.origin}</Typography>
      </Box>
      <Box>
        <Typography>Destination: {props.destination}</Typography>
      </Box>
    </Card>
  );
};
